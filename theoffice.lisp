(load "clm-5/all.lisp")
(compile-file "clm-5/piano.ins")
(load "clm-5/piano")

(with-sound ()

  ; Measure 1
  (p 0 :keynum 57.0 :duration 2)
  (p 0 :keynum 68.0 :duration .25)
  (p 0 :keynum 66.0 :duration .25)
  (p .25 :keynum 64.0 :duration .25)
  (p .5 :keynum 68.0 :duration 1)
  (p .5 :keynum 66.0 :duration 1)
  (p 1.5 :keynum 68.0 :duration .25)
  (p 1.5 :keynum 66.0 :duration .25)
  (p 1.75 :keynum 64.0 :duration .25)

  ; Measure 2
  (p 2 :keynum 59.0 :duration 2)
  (p 2 :keynum 68.0 :duration .25)
  (p 2 :keynum 66.0 :duration .25)
  (p 2.25 :keynum 63.0 :duration .25)
  (p 2.5 :keynum 68.0 :duration 1)
  (p 2.5 :keynum 66.0 :duration 1)
  (p 3.5 :keynum 68.0 :duration .25)
  (p 3.5 :keynum 66.0 :duration .25)
  (p 3.75 :keynum 63.0 :duration .25)

  ; Measure 3
  (p 4 :keynum 55.0 :duration 2)
  (p 4 :keynum 69.0 :duration .25)
  (p 4 :keynum 66.0 :duration .25)
  (p 4.25 :keynum 64.0 :duration .25)
  (p 4.5 :keynum 69.0 :duration 1)
  (p 4.5 :keynum 66.0 :duration 1)
  (p 5.5 :keynum 69.0 :duration .25)
  (p 5.5 :keynum 66.0 :duration .25)
  (p 5.75 :keynum 64.0 :duration .25)

  ; Measure 4
  (p 6 :keynum 60.0 :duration .5)
  (p 6 :keynum 69.0 :duration .5)
  (p 6 :keynum 67.0 :duration .5)
  (p 6.5 :keynum 60.0 :duration .5)
  (p 6.5 :keynum 69.0 :duration .5)
  (p 6.5 :keynum 67.0 :duration .5)
  (p 7 :keynum 60.0 :duration .5)
  (p 7 :keynum 69.0 :duration .25)
  (p 7 :keynum 67.0 :duration .25)
  (p 7.25 :keynum 66.0 :duration .25)
  (p 7.5 :keynum 60.0 :duration .5)
  (p 7.5 :keynum 65.0 :duration .25)
  (p 7.75 :keynum 66.0 :duration .25)

  ; Measure 5
    ; Treble
    (p 8 :keynum 71.0 :duration 1)
    (p 8 :keynum 68.0 :duration .25)
    (p 8 :keynum 66.0 :duration .25)
    (p 8.25 :keynum 64.0 :duration .25)
    (p 8.5 :keynum 68.0 :duration 1)
    (p 8.5 :keynum 66.0 :duration 1)
    (p 9 :keynum 78.0 :duration 2) ; extends into next measure
    (p 9.5 :keynum 68.0 :duration .25)
    (p 9.5 :keynum 66.0 :duration .25)
    (p 9.75 :keynum 64.0 :duration .25)

    ; Bass
    (p 8 :keynum 57.0 :duration .5)
    (p 8.5 :keynum 57.0 :duration .5)
    (p 9 :keynum 57.0 :duration .5)
    (p 9.5 :keynum 57.0 :duration .5)


  ; Measure 6
    ; Treble
    (p 10 :keynum 68.0 :duration .25)
    (p 10 :keynum 66.0 :duration .25)
    (p 10.25 :keynum 63.0 :duration .25)
    (p 10.5 :keynum 68.0 :duration 1.5)
    (p 10.5 :keynum 66.0 :duration 1.5)
    (p 11 :keynum 77.0 :duration .25)
    (p 11.25 :keynum 78.0 :duration .25)
    (p 11.5 :keynum 77.0 :duration .25)
    (p 11.75 :keynum 75.0 :duration .25)

    ; Bass
    (p 10 :keynum 59.0 :duration .5)
    (p 10.5 :keynum 59.0 :duration .5)
    (p 11 :keynum 59.0 :duration .5)
    (p 11.5 :keynum 59.0 :duration .5)

  ; Measure 7
    ; Treble
    (p 12 :keynum 76.0 :duration .25)
    (p 12 :keynum 69.0 :duration .25)
    (p 12.25 :keynum 64.0 :duration .25)
    (p 12.5 :keynum 69.0 :duration 1)
    (p 12.5 :keynum 66.0 :duration 1)
    (p 13.5 :keynum 69.0 :duration .25)
    (p 13.5 :keynum 66.0 :duration .25)
    (p 13.75 :keynum 64.0 :duration .25)


    ; Bass
    (p 12 :keynum 55.0 :duration .5)
    (p 12.5 :keynum 55.0 :duration .5)
    (p 13 :keynum 55.0 :duration .5)
    (p 13.5 :keynum 55.0 :duration .5)

    ; Measure 8
      ; Treble
      (p 14 :keynum 74.0 :duration .5)
      (p 14 :keynum 67.0 :duration .5)
      (p 14.5 :keynum 74.0 :duration .5)
      (p 14.5 :keynum 67.0 :duration .5)
      (p 15 :keynum 74.0 :duration .25)
      (p 15 :keynum 67.0 :duration .25)
      (p 15.25 :keynum 73.0 :duration .25)
      (p 15.25 :keynum 66.0 :duration .25)
      (p 15.5 :keynum 72.0 :duration .25)
      (p 15.5 :keynum 65.0 :duration .25)
      (p 15.75 :keynum 73.0 :duration .25)
      (p 15.75 :keynum 66.0 :duration .25)


      ; Bass
      (p 14 :keynum 60.0 :duration .5)
      (p 14.5 :keynum 60.0 :duration .5)
      (p 15 :keynum 60.0 :duration .5)
      (p 15.5 :keynum 60.0 :duration .5)

    ; Measure 9
      ; Treble
      (p 16 :keynum 71.0 :duration 1)
      (p 16 :keynum 68.0 :duration .25)
      (p 16 :keynum 66.0 :duration .25)
      (p 16.25 :keynum 64.0 :duration .25)
      (p 16.5 :keynum 68.0 :duration 1)
      (p 16.5 :keynum 66.0 :duration 1)
      (p 17 :keynum 78.0 :duration 2) ; extends into next measure
      (p 17.5 :keynum 68.0 :duration .25)
      (p 17.5 :keynum 66.0 :duration .25)
      (p 17.75 :keynum 64.0 :duration .25)


      ; Bass
      (p 16 :keynum 57.0 :duration .5)
      (p 16.5 :keynum 57.0 :duration .5)
      (p 17 :keynum 57.0 :duration .5)
      (p 17.5 :keynum 57.0 :duration .5)

    ; Measure 10
      ; Treble
      (p 18 :keynum 68.0 :duration .25)
      (p 18 :keynum 66.0 :duration .25)
      (p 18.25 :keynum 63.0 :duration .25)
      (p 18.5 :keynum 68.0 :duration 1.5)
      (p 18.5 :keynum 66.0 :duration 1.5)
      (p 19 :keynum 77.0 :duration .25)
      (p 19.25 :keynum 78.0 :duration .25)
      (p 19.5 :keynum 79.0 :duration .25)
      (p 19.75 :keynum 77.0 :duration .25)

      ; Bass
      (p 18 :keynum 59.0 :duration .5)
      (p 18.5 :keynum 59.0 :duration .5)
      (p 19 :keynum 59.0 :duration .5)
      (p 19.5 :keynum 59.0 :duration .5)

    ; Measure 11
      ; Treble
      (p 20 :keynum 76.0 :duration .25)
      (p 20 :keynum 69.0 :duration .25)
      (p 20.25 :keynum 64.0 :duration .25)
      (p 20.5 :keynum 69.0 :duration 1.5)
      (p 20.5 :keynum 66.0 :duration 1.5)
      (p 21 :keynum 75.0 :duration .25)
      (p 21.25 :keynum 76.0 :duration .25)
      (p 21.5 :keynum 75.0 :duration .25)
      (p 21.75 :keynum 73.0 :duration .25)



      ; Bass
      (p 20 :keynum 55.0 :duration .5)
      (p 20.5 :keynum 55.0 :duration .5)
      (p 21 :keynum 55.0 :duration .5)
      (p 21.5 :keynum 55.0 :duration .5)

    ; Measure 12
      ; Treble
      (p 22 :keynum 74.0 :duration .5 :amp .8)
      (p 22 :keynum 67.0 :duration .5 :amp .8)
      (p 22.5 :keynum 74.0 :duration .5 :amp .8)
      (p 22.5 :keynum 67.0 :duration .5 :amp .8)
      (p 23 :keynum 74.0 :duration .25 :amp .8)
      (p 23 :keynum 67.0 :duration .25 :amp .8)
      (p 23.25 :keynum 73.0 :duration .25 :amp .8)
      (p 23.25 :keynum 66.0 :duration .25 :amp .8)
      (p 23.5 :keynum 72.0 :duration .25 :amp .8)
      (p 23.5 :keynum 65.0 :duration .25 :amp .8)
      (p 23.75 :keynum 73.0 :duration .25 :amp .8)
      (p 23.75 :keynum 66.0 :duration .25 :amp .8)



      ; Bass
      (p 22 :keynum 60.0 :duration .5 :amp .8)
      (p 22 :keynum 53.0 :duration .5 :amp .8)
      (p 22.5 :keynum 60.0 :duration .5 :amp .8)
      (p 22.5 :keynum 53.0 :duration .5 :amp .8)
      (p 23 :keynum 60.0 :duration .5 :amp .8)
      (p 23 :keynum 53.0 :duration .5 :amp .8)
      (p 23.5 :keynum 60.0 :duration .5 :amp .8)
      (p 23.5 :keynum 53.0 :duration .5 :amp .8)

    ; Measure 13
      ; Treble
      (p 24 :keynum 71.0 :duration 1 :amp .8)
      (p 24 :keynum 68.0 :duration .25 :amp .8)
      (p 24 :keynum 66.0 :duration .25 :amp .8)
      (p 24.25 :keynum 64.0 :duration .25 :amp .8)
      (p 24.5 :keynum 68.0 :duration 1 :amp .8)
      (p 24.5 :keynum 66.0 :duration 1 :amp .8)
      (p 24.5 :keynum 68.0 :duration .25 :amp .8)
      (p 25 :keynum 78.0 :duration 2 :amp .8)
      (p 25.5 :keynum 66.0 :duration .25 :amp .8)
      (p 25.75 :keynum 64.0 :duration .25 :amp .8)


      ; Bass
      (p 24 :keynum 57.0 :duration .5 :amp .8)
      (p 24 :keynum 50.0 :duration .5 :amp .8)
      (p 24.5 :keynum 57.0 :duration .5 :amp .8)
      (p 24.5 :keynum 50.0 :duration .5 :amp .8)
      (p 25 :keynum 57.0 :duration .5 :amp .8)
      (p 25 :keynum 50.0 :duration .5 :amp .8)
      (p 25.5 :keynum 57.0 :duration .5 :amp .8)
      (p 25.5 :keynum 50.0 :duration .5 :amp .8)

    ; Measure 14
      ; Treble
      (p 26 :keynum 68.0 :duration .25)
      (p 26 :keynum 66.0 :duration .25)
      (p 26.25 :keynum 63.0 :duration .25)
      (p 26.5 :keynum 68.0 :duration 1.5)
      (p 26.5 :keynum 66.0 :duration 1.5)
      (p 27 :keynum 77.0 :duration .25)
      (p 27.25 :keynum 78.0 :duration .25)
      (p 27.5 :keynum 77.0 :duration .25)
      (p 27.75 :keynum 75.0 :duration .25)


      ; Bass
      (p 26 :keynum 59.0 :duration .5 :amp .8)
      (p 26 :keynum 52.0 :duration .5 :amp .8)
      (p 26.5 :keynum 59.0 :duration .5 :amp .8)
      (p 26.5 :keynum 52.0 :duration .5 :amp .8)
      (p 27 :keynum 59.0 :duration .5 :amp .8)
      (p 27 :keynum 52.0 :duration .5 :amp .8)
      (p 27.5 :keynum 59.0 :duration .5 :amp .8)
      (p 27.5 :keynum 52.0 :duration .5 :amp .8)

    ; Measure 15
      ; Treble
      (p 28 :keynum 76.0 :duration .25)
      (p 28 :keynum 69.0 :duration .25)
      (p 28.25 :keynum 64.0 :duration .25)
      (p 28.5 :keynum 69.0 :duration 1)
      (p 28.5 :keynum 66.0 :duration 1)
      (p 29.5 :keynum 69.0 :duration .25)
      (p 29.5 :keynum 66.0 :duration .25)
      (p 29.75 :keynum 74.0 :duration .25)


      ; Bass
      (p 28 :keynum 55.0 :duration .5 :amp .8)
      (p 28 :keynum 48.0 :duration .5 :amp .8)
      (p 28.5 :keynum 55.0 :duration .5 :amp .8)
      (p 28.5 :keynum 48.0 :duration .5 :amp .8)
      (p 29 :keynum 55.0 :duration .5 :amp .8)
      (p 29 :keynum 48.0 :duration .5 :amp .8)
      (p 29.5 :keynum 55.0 :duration .5 :amp .8)
      (p 29.5 :keynum 48.0 :duration .5 :amp .8)

    ; Measure 16
      ; Treble
      (p 30 :keynum 74.0 :duration .5 :amp .8)
      (p 30 :keynum 67.0 :duration .5 :amp .8)
      (p 30.5 :keynum 74.0 :duration .5 :amp .8)
      (p 30.5 :keynum 67.0 :duration .5 :amp .8)
      (p 31 :keynum 74.0 :duration .25 :amp .8)
      (p 31 :keynum 67.0 :duration .25 :amp .8)
      (p 31.25 :keynum 73.0 :duration .25 :amp .8)
      (p 31.25 :keynum 66.0 :duration .25 :amp .8)
      (p 31.5 :keynum 72.0 :duration .25 :amp .8)
      (p 31.5 :keynum 65.0 :duration .25 :amp .8)
      (p 31.75 :keynum 73.0 :duration .25 :amp .8)
      (p 31.75 :keynum 66.0 :duration .25 :amp .8)



      ; Bass
      (p 30 :keynum 60.0 :duration .5 :amp .8)
      (p 30 :keynum 53.0 :duration .5 :amp .8)
      (p 30.5 :keynum 60.0 :duration .5 :amp .8)
      (p 30.5 :keynum 53.0 :duration .5 :amp .8)
      (p 31 :keynum 60.0 :duration .5 :amp .8)
      (p 31 :keynum 53.0 :duration .5 :amp .8)
      (p 31.5 :keynum 60.0 :duration .5 :amp .8)
      (p 31.5 :keynum 53.0 :duration .5 :amp .8)

    ; Measure 17
      ; Treble
      (p 32 :keynum 71.0 :duration 1 :amp .8)
      (p 32 :keynum 68.0 :duration .25 :amp .8)
      (p 32 :keynum 66.0 :duration .25 :amp .8)
      (p 32.25 :keynum 64.0 :duration .25 :amp .8)
      (p 32.5 :keynum 68.0 :duration 1 :amp .8)
      (p 32.5 :keynum 66.0 :duration 1 :amp .8)
      (p 32.5 :keynum 68.0 :duration .25 :amp .8)
      (p 33 :keynum 78.0 :duration 2 :amp .8)
      (p 33.5 :keynum 66.0 :duration .25 :amp .8)
      (p 33.75 :keynum 64.0 :duration .25 :amp .8)


      ; Bass
      (p 32 :keynum 57.0 :duration .5 :amp .8)
      (p 32 :keynum 50.0 :duration .5 :amp .8)
      (p 32.5 :keynum 57.0 :duration .5 :amp .8)
      (p 32.5 :keynum 50.0 :duration .5 :amp .8)
      (p 33 :keynum 57.0 :duration .5 :amp .8)
      (p 33 :keynum 50.0 :duration .5 :amp .8)
      (p 33.5 :keynum 57.0 :duration .5 :amp .8)
      (p 33.5 :keynum 50.0 :duration .5 :amp .8)

    ; Measure 18
      ; Treble
      (p 34 :keynum 68.0 :duration .25)
      (p 34 :keynum 66.0 :duration .25)
      (p 34.25 :keynum 63.0 :duration .25)
      (p 34.5 :keynum 68.0 :duration 1.5)
      (p 34.5 :keynum 66.0 :duration 1.5)
      (p 35 :keynum 77.0 :duration .25)
      (p 35.25 :keynum 78.0 :duration .25)
      (p 35.5 :keynum 79.0 :duration .25)
      (p 35.75 :keynum 77.0 :duration .25)


      ; Bass
      (p 34 :keynum 59.0 :duration .5 :amp .8)
      (p 34 :keynum 52.0 :duration .5 :amp .8)
      (p 34.5 :keynum 59.0 :duration .5 :amp .8)
      (p 34.5 :keynum 52.0 :duration .5 :amp .8)
      (p 35 :keynum 59.0 :duration .5 :amp .8)
      (p 35 :keynum 52.0 :duration .5 :amp .8)
      (p 35.5 :keynum 59.0 :duration .5 :amp .8)
      (p 35.5 :keynum 52.0 :duration .5 :amp .8)

    ; Measure 19
      ; Treble
      (p 36 :keynum 76.0 :duration .25)
      (p 36 :keynum 69.0 :duration .25)
      (p 36.25 :keynum 64.0 :duration .25)
      (p 36.5 :keynum 69.0 :duration 1.5)
      (p 36.5 :keynum 66.0 :duration 1.5)
      (p 37 :keynum 75.0 :duration .25)
      (p 37.25 :keynum 76.0 :duration .25)
      (p 37.5 :keynum 75.0 :duration .25)
      (p 37.75 :keynum 73.0 :duration .25)


      ; Bass
      (p 36 :keynum 55.0 :duration .5 :amp .8)
      (p 36 :keynum 48.0 :duration .5 :amp .8)
      (p 36.5 :keynum 55.0 :duration .5 :amp .8)
      (p 36.5 :keynum 48.0 :duration .5 :amp .8)
      (p 37 :keynum 55.0 :duration .5 :amp .8)
      (p 37 :keynum 48.0 :duration .5 :amp .8)
      (p 37.5 :keynum 55.0 :duration .5 :amp .8)
      (p 37.5 :keynum 48.0 :duration .5 :amp .8)

    ; Measure 20
      ; Treble
      (p 38 :keynum 74.0 :duration .5 :amp .8)
      (p 38 :keynum 67.0 :duration .5 :amp .8)
      (p 38.5 :keynum 74.0 :duration .5 :amp .8)
      (p 38.5 :keynum 67.0 :duration .5 :amp .8)
      (p 39 :keynum 74.0 :duration .25 :amp .8)
      (p 39 :keynum 67.0 :duration .25 :amp .8)
      (p 39.25 :keynum 73.0 :duration .25 :amp .8)
      (p 39.25 :keynum 66.0 :duration .25 :amp .8)
      (p 39.5 :keynum 72.0 :duration .25 :amp .8)
      (p 39.5 :keynum 65.0 :duration .25 :amp .8)
      (p 39.75 :keynum 73.0 :duration .25 :amp .8)
      (p 39.75 :keynum 66.0 :duration .25 :amp .8)



      ; Bass
      (p 38 :keynum 60.0 :duration .5 :amp .8)
      (p 38 :keynum 53.0 :duration .5 :amp .8)
      (p 38.5 :keynum 60.0 :duration .5 :amp .8)
      (p 38.5 :keynum 53.0 :duration .5 :amp .8)
      (p 39 :keynum 60.0 :duration .5 :amp .8)
      (p 39 :keynum 53.0 :duration .5 :amp .8)
      (p 39.5 :keynum 60.0 :duration .5 :amp .8)
      (p 39.5 :keynum 53.0 :duration .5 :amp .8)

    ; Measure 21
      ; Treble
      (p 40 :keynum 64.0 :duration 1 :amp .6)
      (p 41 :keynum 68.0 :duration .25 :amp .6)
      (p 41.25 :keynum 66.0 :duration .25 :amp .6)
      (p 41.5 :keynum 68.0 :duration .25 :amp .6)
      (p 41.75 :keynum 71.0 :duration .25 :amp .6)


      ; Bass
      (p 40 :keynum 57.0 :duration 4 :amp .6)
      (p 40 :keynum 50.0 :duration 4 :amp .6)

    ; Measure 22
      ; Treble
      (p 42 :keynum 73.0 :duration .25 :amp .6)
      (p 42.25 :keynum 71.0 :duration .25 :amp .6)
      (p 42.5 :keynum 73.0 :duration .25 :amp .6)
      (p 42.75 :keynum 75.0 :duration .25 :amp .6)
      (p 43 :keynum 78.0 :duration 1 :amp .6)

 )
