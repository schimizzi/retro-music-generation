(load "clm-5/all.lisp")
(compile-file "clm-5/v.ins")
(compile-file "clm-5/piano.ins")
(load "clm-5/v")
(load "clm-5/piano")

(with-sound ()
  (fm-violin 1 1 196 .10)
	(fm-violin 1 .5 392 .10)
	(fm-violin 1 .5 494 .10)
	(fm-violin 1 .5 587 .10)

  (fm-violin 1.5 .5 392 .10)
  (fm-violin 1.5 .5 494 .10)
  (fm-violin 1.5 .5 587 .10)

  (fm-violin 2 1 220 .10)
  (fm-violin 2 .5 392 .10)
	(fm-violin 2 .5 440 .10)
	(fm-violin 2 .5 523 .10)

  (fm-violin 2.5 .5 392 .10)
  (fm-violin 2.5 .5 440 .10)
  (fm-violin 2.5 .5 523 .10)

  (fm-violin 3 1 294 .10)
  (fm-violin 3 .5 370 .10)
  (fm-violin 3 .5 523 .10)
  (fm-violin 3 .5 587 .10)

  (fm-violin 3.5 .5 370 .10)
  (fm-violin 3.5 .5 523 .10)
  (fm-violin 3.5 .5 587 .10)

  (fm-violin 4 1 294 .10)
  (fm-violin 4 .5 370 .10)
  (fm-violin 4 .5 523 .10)
  (fm-violin 4 .5 587 .10)

  (fm-violin 4.5 .5 370 .10)
  (fm-violin 4.5 .5 523 .10)
  (fm-violin 4.5 .5 587 .10)

  (p 1.55 :duration .25 :keynum 61 :strike-velocity .25)
  (p 1.80 :duration .25 :keynum 61 :strike-velocity .25)
  (p 2.05 :duration .25 :keynum 61 :strike-velocity .25)
  (p 2.30 :duration .25 :keynum 61 :strike-velocity .25)
  (p 2.80 :duration .5 :keynum 61 :strike-velocity .25)

  (fm-violin 5 1 196 .10)
  (fm-violin 5 .5 392 .10)
  (fm-violin 5 .5 494 .10)
  (fm-violin 5 .5 587 .10)

  (fm-violin 5.5 .5 392 .10)
  (fm-violin 5.5 .5 494 .10)
  (fm-violin 5.5 .5 587 .10)

  (fm-violin 6 1 220 .10)
  (fm-violin 6 .5 392 .10)
  (fm-violin 6 .5 440 .10)
  (fm-violin 6 .5 523 .10)

  (fm-violin 6.5 .5 392 .10)
  (fm-violin 6.5 .5 440 .10)
  (fm-violin 6.5 .5 523 .10)

  (fm-violin 7 1 294 .10)
  (fm-violin 7 .5 370 .10)
  (fm-violin 7 .5 523 .10)
  (fm-violin 7 .5 587 .10)

  (fm-violin 7.5 .5 370 .10)
  (fm-violin 7.5 .5 523 .10)
  (fm-violin 7.5 .5 587 .10)

  (fm-violin 8 1 294 .10)
  (fm-violin 8 .5 370 .10)
  (fm-violin 8 .5 523 .10)
  (fm-violin 8 .5 587 .10)

  (fm-violin 8.5 .5 370 .10)
  (fm-violin 8.5 .5 523 .10)
  (fm-violin 8.5 .5 587 .10)

  (p 5.55 :duration .25 :keynum 61 :strike-velocity .25)
  (p 5.80 :duration .25 :keynum 61 :strike-velocity .25)
  (p 6.05 :duration .25 :keynum 61 :strike-velocity .25)
  (p 6.30 :duration .25 :keynum 61 :strike-velocity .25)
  (p 6.80 :duration .5 :keynum 61 :strike-velocity .25)

  (fm-violin 9 1 196 .10)
  (fm-violin 9 .5 392 .10)
  (fm-violin 9 .5 494 .10)
  (fm-violin 9 .5 587 .10)

  (fm-violin 9.5 .5 392 .10)
  (fm-violin 9.5 .5 494 .10)
  (fm-violin 9.5 .5 587 .10)

  (fm-violin 10 1 62 .10)
  (fm-violin 10 .5 392 .10)
  (fm-violin 10 .5 440 .10)
  (fm-violin 10 .5 523 .10)

  (fm-violin 10.5 .5 392 .10)
  (fm-violin 10.5 .5 440 .10)
  (fm-violin 10.5 .5 523 .10)

  (fm-violin 11 1 82 .10)
  (fm-violin 11 .5 196 .10)
  (fm-violin 11 .5 247 .10)
  (fm-violin 11 .5 379 .10)

  (fm-violin 11.5 .5 196 .10)
  (fm-violin 11.5 .5 247 .10)
  (fm-violin 11.5 .5 379 .10)

  (fm-violin 12 1 49 .10)
  (fm-violin 12 .5 185 .10)
  (fm-violin 12 .5 247 .10)
  (fm-violin 12 .5 293 .10)

  (fm-violin 12.5 .5 185 .10)
  (fm-violin 12.5 .5 247 .10)
  (fm-violin 12.5 .5 293 .10)

  (fm-violin 13 1 130 .10)
  (fm-violin 13 .5 440 .10)
  (fm-violin 13 .5 524 .10)
  (fm-violin 13 .5 659 .10)

  (fm-violin 13.5 .5 440 .10)
  (fm-violin 13.5 .5 524 .10)
  (fm-violin 13.5 .5 659 .10)

  (fm-violin 14 1 73 .10)
  (fm-violin 14 .5 370 .10)
  (fm-violin 14 .5 523 .10)
  (fm-violin 14 .5 587 .10)

  (fm-violin 14.5 .5 370 .10)
  (fm-violin 14.5 .5 523 .10)
  (fm-violin 14.5 .5 587 .10)

  (p 9.55 :duration .25 :keynum 61 :strike-velocity .25)
  (p 9.80 :duration .25 :keynum 61 :strike-velocity .25)
  (p 10.05 :duration .25 :keynum 61.5 :strike-velocity .25)
  (p 10.30 :duration .25 :keynum 61.5 :strike-velocity .25)
  (p 10.80 :duration .5 :keynum 62 :strike-velocity .25)
  (p 11.26 :duration 1.1 :keynum 59 :strike-velocity .25)

  (p 13.55 :duration .16 :keynum 57 :strike-velocity .25)
  (p 13.71 :duration .16 :keynum 58 :strike-velocity .25)
  (p 14.87 :duration .16 :keynum 57 :strike-velocity .25)
  (p 14.03 :duration .16 :keynum 55 :strike-velocity .25)
  (p 14.2 :duration .75 :keynum 55 :strike-velocity .25)
)
